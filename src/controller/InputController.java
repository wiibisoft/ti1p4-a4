package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Timer;

import main.Main;
import model.GameModel;
import model.ScoreSystem;
import view.GameScreen;
import view.ScreenManager;
import wiiusej.WiiUseApiManager;
import wiiusej.Wiimote;

public class InputController
{
	private ScreenManager screenManager;
	private GameModel gameModel;
	private KeyboardController keyboardController;
	private WiiInputController wiiInputController;
	private int numberOfPlayers = 0;
	
	public InputController(ScreenManager screenManager, GameModel gameModel)
	{
		this.screenManager = screenManager;
		this.gameModel  = gameModel;
		try
		{
			Wiimote[] wiimotes = WiiUseApiManager.getWiimotes(2, true);
			wiiInputController = new WiiInputController(wiimotes, screenManager, this);
		}
		catch(Exception e)
		{
			
		}  
		
		//ArduinoController arduinoController = new ArduinoController();
		keyboardController = new KeyboardController(this);
		screenManager.addKeyListener(keyboardController);		
	}
	
	public void changeScreen(int index)
	{
		//TODO: Make switching to correct screen.
		screenManager.setScreen(index);
	}
	
	public void rollObject(double roll, int wiiId){
		screenManager.setObjectAngle(roll, wiiId);
	}
	
	public void setObjectPos(int xPos, int yPos, int wiiId){
		screenManager.setObjectPos(xPos, yPos, wiiId);
	}
	
	public void setNumberOfPlayers(int numPlayers)
	{
		numberOfPlayers = numPlayers;
	}
	
	public int getNumberOfPlayers()
	{
		return numberOfPlayers;
	}

	public void ObjectGrabbed(int wiimoteId) {
		screenManager.ObjectGrabbed(wiimoteId);
	}

	public void ObjectReleased(int wiimoteId) {
		screenManager.ObjectReleased(wiimoteId);
	}
	
	public void incrementScoreTest(int score){
		
		gameModel.returnScoreSystem().incrementScore(100);
	}
}
