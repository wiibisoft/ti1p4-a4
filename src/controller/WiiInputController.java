package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.ScreenManager;
import wiiusej.Wiimote;
import wiiusej.values.Orientation;
import wiiusej.wiiusejevents.physicalevents.ExpansionEvent;
import wiiusej.wiiusejevents.physicalevents.IREvent;
import wiiusej.wiiusejevents.physicalevents.MotionSensingEvent;
import wiiusej.wiiusejevents.physicalevents.NunchukEvent;
import wiiusej.wiiusejevents.physicalevents.WiimoteButtonsEvent;
import wiiusej.wiiusejevents.utils.WiimoteListener;
import wiiusej.wiiusejevents.wiiuseapievents.ClassicControllerInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.ClassicControllerRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.DisconnectionEvent;
import wiiusej.wiiusejevents.wiiuseapievents.GuitarHeroInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.GuitarHeroRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.NunchukInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.NunchukRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.StatusEvent;

public class WiiInputController implements WiimoteListener
{
	private Wiimote wiiMote1;
	private Wiimote wiiMote2;
	private InputController inputController;
	private ScreenManager screenManager;
	
	private boolean player1Ready = false;
	private boolean player2Ready = false;
	
	private boolean player1Skips = false;
	private boolean player2Skips = false;

	public WiiInputController(Wiimote[] wiimotes, ScreenManager screenManager, InputController inputController)
	{
		this.inputController = inputController;
		this.screenManager = screenManager;
		
		if(wiimotes.length < 2)
		{
			inputController.setNumberOfPlayers(1);
			wiiMote1 = wiimotes[0];
			wiiMote1.addWiiMoteEventListeners(this);
			wiiMote1.activateMotionSensing();
			wiiMote1.setSensorBarBelowScreen();
			wiiMote1.activateIRTRacking();
		}
		else
		{
			inputController.setNumberOfPlayers(2);
			wiiMote1 = wiimotes[0];
			wiiMote2 = wiimotes[1];
			wiiMote1.addWiiMoteEventListeners(this);
			wiiMote2.addWiiMoteEventListeners(this);
			wiiMote1.activateMotionSensing();
			wiiMote2.activateMotionSensing();
			wiiMote1.setSensorBarBelowScreen();
			wiiMote2.setSensorBarBelowScreen();
			wiiMote1.activateIRTRacking();
			wiiMote2.activateIRTRacking();
		}
	}

	@Override
	public void onButtonsEvent(WiimoteButtonsEvent wiiInput)
	{
		//switch from start to intro
		if(wiiInput.isButtonOnePressed() && wiiInput.isButtonTwoPressed() 
				&& screenManager.getCurrentScreen().returnScreenCode() == 0) 
		{
			if(inputController.getNumberOfPlayers() == 2)
			{
				//check if player 1 is ready
				if(wiiInput.getWiimoteId() == 1)
				{
					player1Ready = true;
				}
				//check if player 2 is ready
				if(wiiInput.getWiimoteId() == 2)
				{
					player2Ready = true;
				}
				
				//check if both players are ready
				if(player1Ready == true && player2Ready == true)
				{
					inputController.changeScreen(1);
				}
			}
			else
			{
				inputController.changeScreen(1);
			}
		}
		
		if(wiiInput.isButtonOneJustReleased() || wiiInput.isButtonTwoJustReleased()	
				&& screenManager.getCurrentScreen().returnScreenCode() == 0) 
		{ 
			if(inputController.getNumberOfPlayers() == 2)
			{
				//check if player 1 isn't ready
				if(wiiInput.getWiimoteId() == 1){
					player1Ready = false;
				}
				//check if player 2 isn't ready
				if(wiiInput.getWiimoteId() == 2){
					player2Ready = false;
				}
			}
		}
		else
		{
			//initiateStart = false;
		}
		
		//skip intro
		
		if(wiiInput.isButtonAPressed() && wiiInput.isButtonBPressed() && screenManager.getCurrentScreen().returnScreenCode() == 1) 
		{		
			if(inputController.getNumberOfPlayers() == 2)
			{
				//check if player 1 is ready
				if(wiiInput.getWiimoteId() == 1)
				{
					player1Skips = true;
				}
				//check if player 2 is ready
				if(wiiInput.getWiimoteId() == 2)
				{
					player2Skips = true;
				}

				//check if both players are ready
				if(player1Skips == true && player2Skips == true)
				{
					inputController.changeScreen(2);
				}
			}
			else
			{
				inputController.changeScreen(2);
			}
		}

		if(wiiInput.isButtonAJustReleased() || wiiInput.isButtonBJustReleased()	&& screenManager.getCurrentScreen().returnScreenCode() == 1) 
		{ 
			if(inputController.getNumberOfPlayers() == 2)
			{
				//check if player 1 isn't ready
				if(wiiInput.getWiimoteId() == 1)
				{
					player1Skips = false;
				}
				//check if player 2 isn't ready
				if(wiiInput.getWiimoteId() == 2)
				{
					player2Skips = false;
				}
			}
		}
		
		if(wiiInput.isButtonAHeld() && screenManager.getCurrentScreen().returnScreenCode() == 2)
		{
			inputController.ObjectGrabbed(wiiInput.getWiimoteId());
		}
		
		if(wiiInput.isButtonAJustReleased() && screenManager.getCurrentScreen().returnScreenCode() == 2)
		{
			inputController.ObjectReleased(wiiInput.getWiimoteId());
		}
	}


	@Override
	public void onClassicControllerInsertedEvent(
			ClassicControllerInsertedEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClassicControllerRemovedEvent(
			ClassicControllerRemovedEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisconnectionEvent(DisconnectionEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onExpansionEvent(ExpansionEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGuitarHeroInsertedEvent(GuitarHeroInsertedEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGuitarHeroRemovedEvent(GuitarHeroRemovedEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onIrEvent(IREvent arg0)
	{
		// TODO Auto-generated method stub
		int x = arg0.getAx();
		int y = arg0.getAy();
		inputController.setObjectPos(x, y, arg0.getWiimoteId());
	}

	@Override
	public void onMotionSensingEvent(MotionSensingEvent arg0)
	{
		// TODO Auto-generated method stub
		Orientation o = arg0.getOrientation();
		double roll = Math.toRadians(o.getRoll());
		inputController.rollObject(roll, arg0.getWiimoteId());
	}

	@Override
	public void onNunchukInsertedEvent(NunchukInsertedEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNunchukRemovedEvent(NunchukRemovedEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusEvent(StatusEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}

}
