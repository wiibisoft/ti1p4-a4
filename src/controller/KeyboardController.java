package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import view.ScreenManager;

public class KeyboardController implements KeyListener 
{

	private InputController inputController;

	public KeyboardController(InputController inputController)
	{
		// TODO Auto-generated constructor stub
		this.inputController = inputController;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		// System.out.println("Testing");
		if(arg0.getKeyCode() == KeyEvent.VK_SPACE)
		{
			inputController.changeScreen(2);
		}
		if(arg0.getKeyCode() == KeyEvent.VK_S){
			inputController.incrementScoreTest(100);
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method 
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}
	
}
