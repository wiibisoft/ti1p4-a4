package main;

import javax.swing.JFrame;

import controller.InputController;
import model.GameModel;
import model.ScoreSystem;
import view.ScreenManager;
import wiiusej.WiiUseApiManager;
import wiiusej.Wiimote;

public class Main extends JFrame
{

	public static void main(String[] args)
	{
	    ScreenManager view = new ScreenManager();
	    GameModel model = new GameModel(view);
	    InputController controller = new InputController(view, model);
	}

}
