package view;

import java.awt.Graphics2D;

import net.phys2d.raw.Body;
import net.phys2d.raw.shapes.Box;

public abstract class WorldObject { // at the creation of a WorldObject a shape must be supplied to the collsionModel for... collision model purposes..
	protected double rotation;
	protected float xPos;
	protected float yPos;
	protected boolean grabable = false;
	private Body collisionModel;
	private int dimension = 2; // 1 for fore ground, 2 for background
//	protected iets plaatje;
	
	public WorldObject(int xPos, int yPos){
		this.xPos = xPos;
		this.yPos = yPos;
	}
	
	public abstract void draw(Graphics2D g2d);
	
	public Body getCollisionModel(){
		return collisionModel;
	}
	
	public void addCollisionModel(int width, int height){
		collisionModel = new Body(new Box(width, height), 10);
	}
	
	public int getDimension(){
		return dimension;
	}
	
	public void update(){ // updates to the, by the physics engine calculated, position and rotation
		xPos = collisionModel.getPosition().getX();
		yPos = collisionModel.getPosition().getY();
		rotation = collisionModel.getRotation();
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public float getxPos() {
		return xPos;
	}

	public void setxPos(float xPos) {
		this.xPos = xPos;
	}

	public float getyPos() {
		return yPos;
	}

	public void setyPos(float yPos) {
		this.yPos = yPos;
	}

	public boolean isGrabable() {
		return grabable;
	}

	public void setGrabable(boolean grabable) {
		this.grabable = grabable;
	}
}