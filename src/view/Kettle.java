package view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import javax.swing.ImageIcon;

public class Kettle extends WorldObject {
	private Shape s1;
	private int hight = 60, width = 160;
	private Point2D position;
	private Color color = new Color(100, 100, 255);
	private Image image;
	
	private int shakeSteps = 0;
	private int shakeLength = 0;
	private boolean shakeLeft = true;

	/*
	 * constructor
	 * 
	 * @ param int posx ; the x position of the mid point of the kettle
	 * 
	 * @ param int posy ; the y position of the mid point of the kettle
	 */
	public Kettle(int posx, int posy) {
		super(posx, posy);
		position = new Point2D.Double(posx, posy);
		// setPosition(posx,posy);

		image = new ImageIcon("res/kettle.png").getImage();
		s1 = new Ellipse2D.Double(posx + image.getWidth(null) / 2 - width / 2,
				posy + hight / 2, width, hight);
	}

	/*
	 * @ param int posx ; the x position of the mid point of the kettle
	 * 
	 * @ param int posy ; the y position of the mid point of the kettle
	 */
	public void setPosition(int posx, int posy) {
		super.setxPos(posx);
		super.setyPos(posy);
		position = new Point2D.Double(posx, posy);
		s1 = new Ellipse2D.Double(posx + image.getWidth(null) / 2 - width / 2,
				posy + hight / 2, width, hight);
	}

	/*
	 * @ return ; the shape to of the kettle
	 */
	public Shape getShape() {
		return s1;
	}

	/*
	 * @ return ; the shape of the image
	 */
	public Image getImageShape() {
		return image;
	}

	/*
	 * @ param Color color ; set color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/*
	 * @param Graphics2D g2d ; the graphic plane to draw in
	 */

	public void draw(Graphics2D g2d) {
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos + 26, yPos + 24);
		transform.rotate(rotation);
		transform.translate(-xPos - 26, -yPos - 24);
		transform.translate(xPos, yPos);

		g2d.setColor(color);
		g2d.fill(s1);

		g2d.drawImage(image, transform, null);
	}

	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	
	public boolean shake(){
		if(shakeLength < 5){
			if(shakeSteps <= 50){
				//shaking right
				this.xPos += 1;
			}
			
			if(shakeSteps > 50 && shakeSteps <=  150){
				if(shakeLeft){
					this.xPos -= 1; 
				}
				else{
					this.xPos += 1;
				}
			}
			
			if(shakeSteps == 50){
				shakeLeft = !shakeLeft;
			}
		
			if(shakeSteps == 150){
				shakeSteps = 50;
				shakeLength += 1;
			}
		}
		if(shakeLength == 5 && shakeSteps == 150){
			shakeSteps = 0;
		}
		if(shakeLength == 5 && shakeSteps < 50){
			this.xPos += 1;
		}
		if(shakeLength == 5 && shakeSteps == 50){
			shakeSteps = 0;
			shakeLength = 0;
			return true;
		}
		shakeSteps += 1;
		return false;
	}
}
