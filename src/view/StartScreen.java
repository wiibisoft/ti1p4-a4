package view;

import java.awt.Graphics;
import java.awt.Graphics2D;

public class StartScreen extends Screen {

	public StartScreen(){
		super(0);
	}

	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.drawString("This is the main screen. Press 1+2 to start", 500, 300);
		
	}

	
}