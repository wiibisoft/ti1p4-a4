package view;

import java.awt.Color;
import java.awt.Graphics;

abstract class FadeInOut {
	protected int x;
	protected int y;
	protected int fadeValue;
	protected Color color;
	
	public FadeInOut(int x, int y, int fadeValue, Color color){
		this.x = x;
		this.y = y;
		this.fadeValue = fadeValue;
		this.color = color;
	}
	
	public abstract boolean update();
	public abstract void draw(Graphics g2d);
	
	
	/**
	 * 
	 * @return true
	 * when increaseAlpha is possible, return true, and update the color of the object.
	 * @return false
	 * when increaseAlpha isn't possible, return false. This is not essential for the function,
	 * but gives you the option to make decisions with the returned value.
	 * 
	 */
	public boolean increaseAlpha(){
		if(color.getAlpha() < 255-fadeValue){
			this.color = new Color(color.getRed(), color.getGreen(), color.getBlue(), (color.getAlpha()+fadeValue));
			return true;
		}
		
		else{
			return false;
		}
	}
	/**
	 * 
	 * @return true
	 * when decreaseAlpha is possible, return true, and update the color of the object.
	 * @return false
	 * when decreaseAlpha isn't possible, return false. This is not essential for the function,
	 * but gives you the option to make decisions with the returned value.
	 * 
	 */
	public boolean decreaseAlpha(){
		if(color.getAlpha() > fadeValue){
			this.color = new Color(color.getRed(), color.getGreen(), color.getBlue(), (color.getAlpha()-fadeValue));
			return true;
		}
		
		else{
			return false;
		}
	}
}
