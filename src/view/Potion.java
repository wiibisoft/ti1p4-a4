package view;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.Image;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

public class Potion extends WorldObject
{
	private int red,green,blue;
	private String name;
	private Shape shape;
	private Image image;
	
	private int volume = 100; // volume in percentages
	
	/*
	 * constructor
	 * @ param String name ; name of the potion
	 * @ param int red ; the number of red the potion contains
	 * @ param int green ; the number of green the potion contains
	 * @ param int blue ; the number of blue the potion contains
	 */
	public Potion (int xPos, int yPos, String name,int red,int green,int blue){
		super(xPos, yPos);
		this.name=name;
		this.red=red;
		this.green=green;
		this.blue=blue;
		this.xPos = xPos;
		this.yPos = yPos;
		
		image = new ImageIcon("res/Potion.png").getImage();
		shape = new Rectangle2D.Double(xPos, yPos, 50, 50);
	}
	
	@Override
	/* Draw the shape */
	public void draw(Graphics2D g2d) {
		
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos+25, yPos+25);
	    transform.rotate(rotation);
	    transform.translate(-xPos-25, -yPos-25);
	    transform.translate(xPos, yPos);
	    
	    g2d.drawImage(image, transform, null);
		
	}
	
	/*
	 * @ return ; the number of red the potion contains
	 */
	public int getRed() {
		return red;
	}

	/*
	 * @ return ; the number of green the potion contains
	 */
	public int getGreen() {
		return green;
	}

	/*
	 * @ return ; the number of blue the potion contains
	 */
	public int getBlue() {
		return blue;
	}
	
	/*
	 * @ return ; the name of the potion
	 */
	public String getName(){
		return name;
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}
	
	public Image getImage() {
		return image;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
}
