package view;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;

import javax.swing.ImageIcon;

import view.WorldObject;

public class Shelf extends WorldObject {

	private Image image;
	private java.awt.Shape shape; //

	public Shelf(int xPos, int yPos, int number) {
		super(xPos, yPos);

		if (number == 1) {
			image = new ImageIcon("res/plankL.png").getImage();
		}
		if (number == 2) {
			image = new ImageIcon("res/plank.png").getImage();
		}
		if (number == 3) {
			image = new ImageIcon("res/plankR.png").getImage();
		}
	}

	@Override
	public void draw(Graphics2D g2d) {
		// TODO Auto-generated method stub
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos + 26, yPos + 24);
		transform.rotate(rotation);
		transform.translate(-xPos - 26, -yPos - 24);
		transform.translate(xPos, yPos);

		g2d.drawImage(image, transform, null);
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
}
