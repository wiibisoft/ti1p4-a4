package view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

public class Hand extends WorldObject {
	private Rectangle2D.Double shape;
	private Image image;

	public Hand(int xPos, int yPos, int number) {
		super(xPos, yPos);

		if (number == 1) {
			image = new ImageIcon("res/Hand_L.png").getImage();
		}
		if (number == 2) {
			image = new ImageIcon("res/Hand_R.png").getImage();
		}
		shape = new Rectangle2D.Double(xPos, yPos, 50, 50);

	}

	public void draw(Graphics2D g2d) {
		AffineTransform transform = new AffineTransform();
		transform.translate(xPos + 26, yPos + 24);
		transform.rotate(rotation);
		transform.translate(-xPos - 26, -yPos - 24);
		transform.translate(xPos, yPos);

		g2d.drawImage(image, transform, null);
	}

	public void setImage(Image image) {
		this.image = image;
	}
}