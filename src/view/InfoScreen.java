package view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class InfoScreen extends Screen {
	
	private BufferedImage introImage = new BufferedImage(240,240, BufferedImage.TYPE_INT_ARGB);
	
	public InfoScreen(){
		super(1);
		try {
			introImage = ImageIO.read(new File("res/intro.png"));
			
		} catch (IOException e) {
			System.out.println("intro.gif was not found in the res folder");
		}
	}
	
public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.drawImage(introImage, 0, 0, null);
	}
}
