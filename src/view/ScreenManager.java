package view;

import java.awt.Panel;
import java.util.ArrayList;







import java.util.List;

import javax.swing.JFrame;

public class ScreenManager extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Screen> Screens = new ArrayList<Screen>();
	private Screen currentScreen = new Screen(99); //default screenCode, only used for constructing.
	

	public ScreenManager(){
		
		Screens.add(new StartScreen());
		Screens.add(new InfoScreen());
		Screens.add(new GameScreen());
		Screens.add(new ScoreScreen());

		
		setTitle("Brewing Magic");
		setSize(1280,720);
		//adds view to the frame.
		setContentPane(currentScreen);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setFocusable(true);
		
		setScreen(0);
		
	}
	
	public void setScreen(int index){
		currentScreen = Screens.get(index);
		setContentPane(currentScreen);
		repaint();
	}

	public Screen getCurrentScreen()
	{
		// TODO Auto-generated method stub
		
		
		return currentScreen;
	}
	
	public void setObjectAngle(double angle, int wiiId)
	{
		if(currentScreen instanceof GameScreen)
		{
			GameScreen s = (GameScreen) currentScreen;
			s.setAngle(angle, wiiId);
//			s.getHand().setRotation(roll);
//			s.repaint();
			
		}
	}
	
	public void setObjectPos(int xPos, int yPos, int wiiId)
	{
		if(currentScreen instanceof GameScreen)
		{
			GameScreen s = (GameScreen) currentScreen;
			s.setPosotion(xPos, yPos, wiiId);
//			GameScreen s = (GameScreen) currentScreen;
//			s.getHand().setxPos(xPos);
//			s.getHand().setyPos(yPos);
//			s.repaint();
		}
	}

	public void ObjectGrabbed(int wiimoteId) {
		if(currentScreen instanceof GameScreen)
		{
			GameScreen s = (GameScreen) currentScreen;
			s.ObjectGrabbed(wiimoteId);
		}
	}

	public void ObjectReleased(int wiimoteId) {
		if(currentScreen instanceof GameScreen)
		{
			GameScreen s = (GameScreen) currentScreen;
			s.ObjectReleased(wiimoteId);
		}
	}
	
	public void addNewWorldObject(WorldObject object)
	{
		if(currentScreen instanceof GameScreen)
		{
			GameScreen screen = (GameScreen) currentScreen;
			screen.addNewWorldObject(object);
		}
	}
	
	public List<WorldObject> getWorldObjects(){
		if(currentScreen instanceof GameScreen){
			GameScreen screen = (GameScreen) currentScreen;
			return screen.getWorldObjects();
		}
		else{
			return null;
		}
	} 
	
	public Screen getSpecificScreen(int index){
		return Screens.get(index);
	}
}