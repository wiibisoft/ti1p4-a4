package view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class Screen extends JPanel {
	
	private int screenCode;

	public Screen(int screenCode) {
		this.screenCode = screenCode;
		setSize(1280,720);
		setVisible(true);
		setFocusable(true);
	}
	
	public int returnScreenCode(){
		return screenCode;
	}
}
