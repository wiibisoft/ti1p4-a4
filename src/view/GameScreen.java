package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.Timer;

public class GameScreen extends Screen implements ActionListener{
	private int score = 0;
	private List<WorldObject> worldObjects = new LinkedList<WorldObject>();
	private Hand handL = null;
	private Hand handR = null;
	private Kettle kettle = null;
	private Timer timer = new Timer(1000/60, this);
	private boolean holdingL = false; // used for checking if hand contains a
										// potion
	private boolean holdingR = false;

	private boolean rewarded = false;
	private BufferedImage backgroundImage = new BufferedImage(240, 240,
			BufferedImage.TYPE_INT_ARGB);

	private ArrayList<FadeInOut> fadeObjects = new ArrayList<FadeInOut>();

	public GameScreen() {
		super(2);
		try {
			backgroundImage = ImageIO.read(new File(
					"res/lighted_background.png"));

		} catch (IOException e) {
			System.out.println("intro.gif was not found in the res folder");
		}
		
		timer.start();
		
		
//		handL = new Hand(100,300,1);
//		worldObjects.add(handL);
//		handR = new Hand(800,300,2);
//		worldObjects.add(handR);
		
		Shelf shelf1 = new Shelf(28,265, 1); // 1 = l , 2 = mid , 3 = Rechts
		worldObjects.add(shelf1);
		Shelf shelf2 = new Shelf(800, 465, 3);
		worldObjects.add(shelf2);

		// potions
		Potion potion1 = new Potion(200, 200, "plsh1", 139, 0, 139);
		worldObjects.add(potion1);
		Potion potion2 = new Potion(300, 200, "plsh2", 0, 0, 255);
		worldObjects.add(potion2);
		Potion potion3 = new Potion(400, 200, "plsh3", 0, 255, 0);
		worldObjects.add(potion3);

		Potion potion4 = new Potion(800, 400, "plsh4", 139, 0, 139);
		worldObjects.add(potion4);
		Potion potion5 = new Potion(900, 400, "plsh5", 0, 0, 255);
		worldObjects.add(potion5);
		Potion potion6 = new Potion(1000, 400, "plsh6", 0, 255, 0);
		worldObjects.add(potion6);

		kettle = new Kettle(580, 480);
		worldObjects.add(kettle);

		handL = new Hand(100, 300, 1);
		worldObjects.add(handL);
		handR = new Hand(800, 300, 2);
		worldObjects.add(handR);
	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.drawImage(backgroundImage, 0, 0, null);

		for (WorldObject object : worldObjects) {
			object.draw(g2d);
		}
		g2d.setFont(new Font("Jokerman", Font.PLAIN, 25));
		g2d.setColor(Color.YELLOW);
		g2d.drawString("Score: "+ score, 25, 25);
		
		for(FadeInOut fadeObject : fadeObjects){
			fadeObject.update();
			fadeObject.draw(g2d);
		}

	}

	public void setAngle(double angle, int wiiId) {
		if (wiiId == 1) {
			handL.setRotation(angle);
			repaint();
		} else if (wiiId == 2) {
			handR.setRotation(angle);
			repaint();
		}
	}

	public void setPosotion(int xPos, int yPos, int wiiId) {
		if (wiiId == 1) {
			handL.setxPos(xPos);
			handL.setyPos(yPos);
			repaint();
		} else if (wiiId == 2) {
			handR.setxPos(xPos);
			handR.setyPos(yPos);
			repaint();
		}
	}

	public void ObjectGrabbed(int wiiId) {
		if (wiiId == 1) {
			Point2D point = new Point2D.Double(handL.getxPos(), handL.getyPos());

			for (WorldObject object : worldObjects) {
				if (object instanceof Potion) {
					Potion p = (Potion) object;
					if (p.getShape().contains(point) && holdingL == false) {
						p.setGrabable(true);
					}
					if (p.isGrabable()) {
						Rectangle2D potion = (Rectangle2D) p.getShape();
						potion.setRect(handL.getxPos(), handL.getyPos(), 50, 50);
						p.setxPos(handL.getxPos());
						p.setyPos(handL.getyPos());
						p.setRotation(handL.getRotation());
						holdingL = true;
						repaint();
					}
				}
			}
		} else if (wiiId == 2) {
			Point2D point = new Point2D.Double(handR.getxPos(), handR.getyPos());

			for (WorldObject object : worldObjects) {
				if (object instanceof Potion) {
					Potion p = (Potion) object;
					if (p.getShape().contains(point) && holdingR == false) {
						p.setGrabable(true);
					}
					if (p.isGrabable()) {
						Rectangle2D potion = (Rectangle2D) p.getShape();
						potion.setRect(handR.getxPos(), handR.getyPos(), 50, 50);
						p.setxPos(handR.getxPos());
						p.setyPos(handR.getyPos());
						p.setRotation(handR.getRotation());
						holdingR = true;
						repaint();
					}
				}
			}
		}
	}

	public void ObjectReleased(int wiiId) {
		if (wiiId == 1) {
			Point2D point = new Point2D.Double(handL.getxPos(), handL.getyPos());

			for (WorldObject object : worldObjects) {
				if (object instanceof Potion) {
					Potion p = (Potion) object;
					p.setGrabable(false);
					holdingL = false;
				}
			}
		} else if (wiiId == 2) {
			Point2D point = new Point2D.Double(handR.getxPos(), handR.getyPos());

			for (WorldObject object : worldObjects) {
				if (object instanceof Potion) {
					Potion p = (Potion) object;
					p.setGrabable(false);
					holdingR = false;
				}
			}
		}
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public void addFadeObject(FadeInOut fadeObject){
		fadeObjects.add(fadeObject);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		//iterator to decide if a fading object will be removed or not.
		Iterator<FadeInOut> fadeInOutIterator = fadeObjects.iterator();
		while(fadeInOutIterator.hasNext()){
			if(fadeInOutIterator.next().update()){
			
			}
			else{
				fadeInOutIterator.remove();
			}
		}
		repaint();
	}

	public void addNewWorldObject(WorldObject object){
		worldObjects.add(object);
		System.out.println("added");
	}
	
	public List<WorldObject> getWorldObjects(){
		return worldObjects;
	}
	
	public Kettle getKettle(){
		return kettle;
	}
}
