package view;

import java.awt.Graphics;
import java.awt.Graphics2D;

public class ScoreScreen extends Screen {
	
	public ScoreScreen(){
		super(3);
	}
	
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.drawString("This screen will display the scores", 500, 300);
		
	}
}
