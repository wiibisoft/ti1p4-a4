package model;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;

import javax.swing.Timer;

import view.GameScreen;
import view.PointsPopup;
import view.ScreenManager;

public class ScoreSystem implements ActionListener {
	private int totalScore = 0;
	private int timeCounter = 0;
	private int decrementSeverity = 1;
	private int timeBorder = 300;
	private Timer timer = new Timer(1000/10, this);
	
	private ScreenManager screenManager = null;
	
	/**
	 * @param startingScore
	 * startingScore wordt meegegeven om dit als een waarde te houden die eventueel
	 * in de toekomst gewijzigd kan worden, handig voor testen. 
	 */
	public ScoreSystem(int startingScore, ScreenManager screenManager){
		this.screenManager = screenManager;
		totalScore = startingScore;
		timer.start();

	}
	
	
	/**
	 * Deze klasse checkt of boolean active true is.
	 * Als dit niet het geval is zal puntenaftrek op score toegepast worden.
	 */
	public void checkScoreDecremention(){
		
		if(timeCounter > timeBorder){
			totalScore -= decrementSeverity;
		}
	}
	
	
	
	/**
	 * @param score
	 * Geef een score mee, de huidige score wordt hiermee verhoogt
	 */
	public void incrementScore(int score){
		
		totalScore += score;
		if(screenManager.getCurrentScreen().returnScreenCode() == 2){
			GameScreen gameScreen = (GameScreen) screenManager.getCurrentScreen();
			
			//make a fade object for the given score. 
			gameScreen.addFadeObject(new PointsPopup(500, 500, 10, Color.YELLOW, score));
			resetTimeCounter();
		}		
		
	}
	
	
	/**
	 * @return
	 * returned een score die geprint kan worden in view.
	 */
	public int getScore(){
		return totalScore;
	}

	/**
	 * Reset functie voor timeCounter variabele.
	 * Kettle zal deze aanroepen als een potion aangemaakt is.
	 */
	public void resetTimeCounter(){
		timeCounter = 0;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		timeCounter++;
		checkScoreDecremention();
		
		if(screenManager.getCurrentScreen().returnScreenCode() == 2){
			GameScreen gameScreen = (GameScreen) screenManager.getCurrentScreen();
			gameScreen.setScore(totalScore);
			gameScreen.repaint();
		}
	}
	
}
