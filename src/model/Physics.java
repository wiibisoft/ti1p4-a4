package model;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Timer;

import view.WorldObject;
import net.phys2d.*;
import net.phys2d.math.Vector2f;
import net.phys2d.raw.Body;
import net.phys2d.raw.World;
import net.phys2d.raw.strategies.QuadSpaceStrategy;

// Deze physics engine zal de referenties naar de body objecten in WorldObject klasse gebruiken
// en vervolgende de WorldObject.Update() methode aanroepn.

public class Physics implements ActionListener {
		// Add World object
	private World physics = new World( 
			new Vector2f(0.0f, 10.0f), 
			50,
			new QuadSpaceStrategy(20,5));
		
	private ArrayList<WorldObject> worldObjects = new ArrayList<WorldObject>();
	
	public Physics(){
		physics.clear();
		physics.setGravity(0, 10);
		
		Timer timer = new Timer(1000/30, this); // 30 gravity calculations per second
		timer.start();
	}
	
	public void addWorldObject(WorldObject newObject){
		worldObjects.add(newObject);
		physics.clear();
		
		for (WorldObject object : worldObjects){
			if (object.getDimension() == 2){
				physics.add(object.getCollisionModel()); //add the collision model of the object to the engine
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		physics.step();
		for (WorldObject object : worldObjects){
			object.update(); // update X,Y and rotation coordinates to the new calculated ones.
		}
	}
}
