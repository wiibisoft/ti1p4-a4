package model;

import java.util.ArrayList;
import java.util.List;

import view.GameScreen;
import view.Kettle;
import view.Potion;
import view.Screen;
import view.ScreenManager;
import view.Shelf;
import view.WorldObject;

public class GameModel
{
	private ScreenManager view;
	private ScoreSystem scoreSystem = null;
	private Physics physics = null;
	private List<WorldObject> worldObjects;
	private List<Potion> createdPotions;
	private Mixer mixer;
	
	public GameModel(ScreenManager screenManager){
		scoreSystem = new ScoreSystem(300, screenManager);
		createdPotions = new ArrayList<Potion>();
		worldObjects = new ArrayList<WorldObject>();
		physics = new Physics();
		view = screenManager;
		
//		List<WorldObject> x = view.getWorldObjects();
//		
//		for(WorldObject object : x){
//			worldObjects.add(object);
//		}
//		
//		for (WorldObject object : worldObjects) {
//			if(object instanceof Potion){
//				object.addCollisionModel(50, 50);
//			}
//			if(object instanceof Shelf){
//				object.addCollisionModel(200, 50);
//			}
//			if(object instanceof Kettle){
//				object.addCollisionModel(100, 100);
//			}
//		}

		GameScreen gameScreen = (GameScreen)view.getSpecificScreen(2);
		mixer = new Mixer(gameScreen.getKettle(), scoreSystem);


	}
	
	public void addPotion(Potion potion){
		if(createdPotions.size() < 10){
			createdPotions.add(potion);
			view.addNewWorldObject(potion);
		}
		
		if(createdPotions.size() == 10){
			view.setScreen(3);
		}
	}
	
	public ScoreSystem returnScoreSystem(){
		return scoreSystem;
	}
}
