package model;

import java.awt.Color;

import view.Kettle;
import view.Potion;
import view.RewardPotion;

public class Mixer {

	private Kettle kettle;
	private int red = 0, green = 0, blue = 0, tolerantie = 2, shakespeed = 100;
	private boolean easter = false;
	private long[] colors = new long[4]; // 0 red ; 1 geen ; 2 blue ; 4 number
											// of drups
	private long time1 = System.currentTimeMillis(), time2 = System
			.currentTimeMillis();
	private boolean p1Hold = false, p2Hold = false, p1Moves = false,
			p2Moves = false, p1Shaking = false, p2Shaking = false;
	private ScoreSystem scoreSystem;
	private EasterList easterList = new EasterList();

	public Mixer(Kettle kettle, ScoreSystem scoreSystem) {
		this.kettle = kettle;
		this.scoreSystem = scoreSystem;
	}

	/*
	 * @ param Potion newone ; the potion that has been added
	 * 
	 * @ param int value ; the number of added drops
	 */
	public void addPotion(Potion newone, int value) {
		colors[0] += newone.getRed();
		colors[1] += newone.getGreen();
		colors[2] += newone.getBlue();
		colors[3] += value;
		kettle.setColor(new Color((int) (colors[0] / colors[3]),
				(int) (colors[1] / colors[3]), (int) (colors[2] / colors[3])));
	}

	/*
	 * @ return ; the number of red the potion contains
	 */
	public int getRed() {
		return red;
	}

	/*
	 * @ return ; the number of green the potion contains
	 */
	public int getGreen() {
		return green;
	}

	/*
	 * @ return ; the number of blue the potion contains
	 */
	public int getBlue() {
		return blue;
	}

	public long[] getColors() {
		return colors;
	}

	/*
	 * @ return ; the total number of drops in the kettle
	 */
	public long howFull() {
		return colors[3];
	}

	/*
	 * the formule on how to calculate
	 */
	public void calculateColor() {
		this.red = (int) (colors[0] / colors[3]);
		this.green = (int) (colors[1] / colors[3]);
		this.blue = (int) (colors[2] / colors[3]);

		if (red != 0 && green != 0 && blue != 0)
			kettle.setColor(new Color(100, 100, 255));
		else
			kettle.setColor(new Color(red, green, blue));
	}

	public boolean bothHands() {
		if (p1Hold && p2Hold)
			return true;
		return false;
	}

	/*
	 * the mixing check + point setter
	 */
	public Potion mixIt() {
		int checkRed = 0;
		int checkGreen = 0;
		int checkBlue = 0;
		boolean easter = false;
		int easterId = -1;
		boolean found = false;

		calculateColor();
		scoreSystem.incrementScore(red + green + blue);
		for (int idx = 0; idx < easterList.numberOfEasters(); idx++) {
			if (!found) {
				easterList.newEaster(idx);
				checkRed = easterList.getRed();
				checkGreen = easterList.getgreen();
				checkBlue = easterList.getblue();

				if (red >= checkRed - tolerantie
						&& red <= checkRed + tolerantie) {
					if (green >= checkGreen - tolerantie
							&& green <= checkGreen + tolerantie) {
						if (blue >= checkBlue - tolerantie
								&& blue <= checkBlue + tolerantie) {
							easter = true;
							easterId = easterList.getEaster();
							scoreSystem.incrementScore(easterList.getPoints());
							easterList.removeEaster(idx);
							found = true;
						}
					}
				}
			}
		}

		if (easter) {
			// play the easterId easter
		}

		for (int i = 0; i < 4; i++) {
			colors[i] = 0;
		}

		
		return new RewardPotion(0, 0, "rewardPotion" + red + green + blue,
				checkRed, checkGreen, checkBlue);
	}

	/*
	 * returnt of het wel of geen easter is
	 */
	public boolean checkEaster() {
		if (easter) {
			easter = false;
			return true;
		}
		return false;
	}

	/*
	 * @ param int player ; the player that cliks on it is set on true for
	 * picking the kettle up
	 */
	public void pickup(int player) {
		switch (player) {
		case 0:
			p1Hold = true;
			break;
		case 1:
			p2Hold = true;
			break;
		default:
			break;
		}
	}

	/*
	 * @ param int player ; the player that cliks on it is set on kettle
	 * released
	 */
	public void release(int player) {
		switch (player) {
		case 0:
			p1Hold = false;
			break;
		case 1:
			p2Hold = false;
			break;
		default:
			break;
		}
	}

	public Potion moveKettle(double x, int player) {
		if (p1Hold && p2Hold) {
			switch (player) {
			case 1:
				if ((x > 100 && p1Moves) || (x < -100 && !p1Moves)) {
					p1Moves = !p1Moves;
					if (System.currentTimeMillis() - time1 <= shakespeed) {
						time2 = System.currentTimeMillis();
						p1Shaking = true;
					}
				}
				if (System.currentTimeMillis() - time1 > shakespeed) {
					time2 = System.currentTimeMillis();
					p1Shaking = false;
				}

				break;
			case 2:
				if ((x > 100 && p2Moves) || (x < -100 && !p2Moves)) {
					p2Moves = !p2Moves;
					if (System.currentTimeMillis() - time2 <= shakespeed) {
						time2 = System.currentTimeMillis();
						p2Shaking = true;
					}
				}
				if (System.currentTimeMillis() - time2 > shakespeed) {
					time2 = System.currentTimeMillis();
					p2Shaking = false;
				}

				break;
			default:
				break;
			}
			

			if (p1Shaking && p2Shaking){
				if (kettle.shake()){
					return mixIt();
				}
			}
		}
		return null;
	}
}
