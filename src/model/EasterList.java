package model;

import java.util.ArrayList;

public class EasterList {
	int[] easter = new int[5];
	private ArrayList<int[]> easters=new ArrayList<int[]>();
	
	public EasterList(){
		fillList();
	}
	
	public void fillList(){
		int[] easter = new int[5];
		easters.clear();

		easter[0]=0; //red
		easter[1]=0; //green
		easter[2]=0; //blue
		easter[3]=1; //eastercount
		easter[4]=1000;// points for this easter
		easters.add(easter);
	}
	
	public void removeEaster(int idx){
		easters.remove(idx);
	}
	
	public void newEaster(int index){
		easter=easters.get(index);
	}

	public int getRed(){
		return easter[0];
	}
	public int getgreen(){
		return easter[1];
	}
	public int getblue(){
		return easter[2];
	}

	public int getEaster(){
		return easter[3];
	}
	
	public int getPoints(){
		return easter[4];
	}
	
	public int numberOfEasters(){
		return easters.size();
	}
}
